#include <iostream>
#include "help.hpp"

namespace steganography {
    void Help::show_help() {
        std::cout << "Dostepne flagi: " << std::endl;
        std::cout << "-i --info <path>" << std::endl;
        std::cout << "          sprawdza czy sciezka prowadzi do pliku o obslugiwanym formacie." << std::endl;
        std::cout << "          wyswietla informacje o formacie, wielkosci obrazu, zajmowane" << std::endl;
        std::cout << "          miejsce w pamieci oraz timestamp ostatniej modyfikacji." << std::endl;
        std::cout << "-e --encrypt <path> <message>" << std::endl;
        std::cout << "          zapisuje w pliku sprecyzowana wiadomosc." << std::endl;
        std::cout << "-d --decrypt <path>" << std::endl;
        std::cout << "          odczytuje wiadomosc zapisana w pliku." << std::endl;
        std::cout << "-c --check <path> <message>" << std::endl;
        std::cout << "          sprawdza czy w danym pliku moglaby zostac zapisana ukryta wiadomosc" << std::endl;
        std::cout << "-h --help - wyswietla pomoc." << std::endl;
    }
}