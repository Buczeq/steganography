#include <iostream>
#include "flag.hpp"
#include "help.hpp"
#include "extensions/Image.hpp"

namespace steganography {

    /**
     * Sprawdza czy ilość podanych argumentów odpowiada ilości wymaganych argumentów.
     * @param argc Liczba podanych argumentów
     * @param required Liczba wymaganych argumentów
     */
    void check_args_amount(int argc, int required) {
        if (argc != required) {
            std::cerr << "Niepoprawna ilosc argumentow. Uzyj flagi -h aby wyswielic pomoc.";
            exit(1);
        }
    }

    /**
     * Na podstawie podanej flagi, sprawdza ilość podanych i wymaganych argumentów oraz wywołuje odpowiednie metody.
     * @param argc Ilość argumentów wywołania aplikacji
     * @param argv Tablica argumentów wywołania aplikacji
     */
    void resolve_arguments(int argc, char *argv[]) {
        if (argc < 2) {
            Help::show_help();
            return;
        }
        switch (Flag::get_argument(argv[1])) {
            case info: {
                check_args_amount(argc, 3);
                auto image = Image::create_image(argv[2]);
                image->show_info();
                delete image;
                break;
            }
            case encrypt: {
                check_args_amount(argc, 4);
                auto image = Image::create_image(argv[2]);
                image->encrypt_message(argv[3]);
                delete image;
                break;
            }
            case decrypt: {
                check_args_amount(argc, 3);
                auto image = Image::create_image(argv[2]);
                image->decrypt_message();
                delete image;
                break;
            }
            case check: {
                check_args_amount(argc, 4);
                auto image = Image::create_image(argv[2]);
                image->check(argv[3]);
                delete image;
                break;
            }
            case help:
                check_args_amount(argc, 2);
                Help::show_help();
                break;
            default: {
                std::cerr << "Podany argument jest niepoprawny. Uzyj flagi -h aby wyswielic pomoc.";
            }
        }
    }


}

int main(int argc, char *argv[]) {
    steganography::resolve_arguments(argc, argv);
    return 0;
}