#ifndef STEGANOGRAPHY_HELP_HPP
#define STEGANOGRAPHY_HELP_HPP
namespace steganography {
    class Help {
    public:
        /**
         * Wyświetla listę flag i aliasów wraz z opisami
         */
        static void show_help();
    };

}

#endif //STEGANOGRAPHY_HELP_HPP
