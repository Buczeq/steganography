
#include <unordered_map>
#include <string>

#ifndef STEGANOGRAPHY_CONFIG_HPP
#define STEGANOGRAPHY_CONFIG_HPP
namespace steganography {
    /**
     * Typy flag z którymi można uruchomić aplikacje.
     */
    enum Flag_type {
        info, encrypt, decrypt, check, help
    };

    class Flag {
        /**
        * @return Mapa obsługiwanych flag jako test oraz odpowiednik jako enum
        */
        static std::unordered_map<std::string, Flag_type> get_arguments();
    public:
        /**
         * Wyszukuje typ flagi na podstawie flagi w formie tekstowej lub aliasu
         * @param arg_as_text Flaga lub alias w formie tekstowej
         * @return Flaga jako enum
         */
        static Flag_type get_argument(const std::string &arg_as_text);
    };


}
#endif //STEGANOGRAPHY_CONFIG_HPP
