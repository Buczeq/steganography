#include <unordered_map>
#include <iostream>
#include "Image.hpp"
#include "png.hpp"
#include "bmp.hpp"
#include <sys/stat.h>
#include <bitset>

namespace steganography {

    std::unordered_map<std::string, Extension_type> Image::get_extension_types() {
        std::unordered_map<std::string, Extension_type> extension_types = {
                std::make_pair("png", Extension_type::png),
                std::make_pair("bmp", Extension_type::bmp),
        };
        return extension_types;
    }

    Extension_type Image::get_extension_as_enum(const std::string &path) {
        return get_extension(path).second;
    }

    std::string Image::get_extension_as_string(const std::string &path) {
        return get_extension(path).first;
    }

    std::pair<const std::basic_string<char>, Extension_type> Image::get_extension(const std::string &path) {
        auto extension_types = get_extension_types();
        auto extension_as_text = path.substr(path.find_last_of('.') + 1);
        for (auto &c: extension_as_text) {
            c = tolower(c);
        }
        auto pair = extension_types.find(extension_as_text);
        if (pair != extension_types.end()) {
            std::pair p(pair->first, pair->second);
            return p;
        } else {
            std::cerr << "Sciezka prowadzi do pliku o nieobslugiwanym formacie." << std::endl;
            exit(1);
        }
    }

    void Image::show_info() {
        std::cout << "Format pliku: " << get_extension_as_string(path) << std::endl;
        std::cout << "Zajmowane miejsce w pamieci: " << get_file_size() << " bajtow" << std::endl;
        std::cout << "Timestamp ostatniej modyfikacji: " << get_modification_timestamp(path) << std::endl;
        int width;
        int height;
        get_image_dimensions(width, height);
        std::cout << "Wielkosc obrazu: " << width << "x" << height << "px" << std::endl;
    }



    Image *Image::create_image(const std::string &path) {
        switch (get_extension_as_enum(path)) {
            case Extension_type::png:
                return new PNG(path);
            case Extension_type::bmp:
                return new BMP(path);
            default: {
                std::cerr << "Rozszerzenie nie jest obsługiwane." << std::endl;
                exit(1);
            }
        }
    }



    __time64_t Image::get_modification_timestamp(const std::string &path) {
        struct stat result{};
        if (stat(path.c_str(), &result) == 0) {
            auto mod_time = result.st_mtime;
            return mod_time;
        } else {
            return -1;
        }
    }

    std::string Image::convert_string_to_binary_string(const std::string &message) {
        std::string binaryString;
        for (char _char: message) {
            binaryString += std::bitset<8>(_char).to_string();
        }
        return binaryString;
    }

    std::streamsize Image::get_file_size() {
        file.seekg(0, std::ios::end);
        std::streamsize size = file.tellg();
        file.clear();
        return size;
    }

    Image::Image(const std::string &path) {
        this->path = path;
        this->file.open(path.c_str(), std::ios::out | std::ios::in | std::ios::binary);
        if (!file.good()) {
            std::cerr << "Plik nie istnieje lub nie masz uprawnien aby go odczytac." << std::endl;
            exit(1);
        }
        this->buffer = std::vector<unsigned char>(std::istreambuf_iterator<char>(file), {});
    }

    Image::~Image() {
        this->file.close();
    }

}