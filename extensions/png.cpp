#include <iostream>
#include <sstream>
#include <bitset>
#include "png.hpp"

namespace steganography {

    const std::string KEYWORD = "s23156";
    const std::string CHUNK_NAME = "tEXt";
    static constexpr size_t NULL_BYTES_AFTER_KEYWORD = 1;
    static constexpr size_t NULL_BYTES_BEFORE_CHUNK = 4;
    static unsigned char null = '\0';

    /**
     * Zapisuje podany znak do pliku.
     * @param file Strumień pliku
     * @param letter Litera
     * @param quantity Ilość powtórzeń zapisu
     */
    void write_text_to_file(std::fstream &file, unsigned char &letter, int quantity = 1) {
        for (int i = 0; i < quantity; i++) {
            file.write(reinterpret_cast<char *>(&letter), sizeof(letter));

        }
    }

    /**
     * Zapisuje podany tekst do pliku.
     * @param file Strumień pliku
     * @param text text
     * @param quantity Ilość powtórzeń zapisu
     */
    void write_text_to_file(std::fstream &file, const std::string &text, int quantity = 1) {
        for (int i = 0; i < quantity; i++) {
            for (auto &letter: text) {
                file.write(&letter, sizeof(letter));
            }
        }
    }

    int PNG::find_text_index(const std::string &text, const std::vector<unsigned char> &buffer) {
        for (int i = 0; i < buffer.size() - text.size(); i++) {
            bool flag = false;
            for (int j = 0; j < text.size(); j++) {
                if (buffer[i + j] != text[j]) {
                    flag = false;
                    break;
                }
                flag = true;
            }
            if (flag) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Tworzy chunk tekstowy wraz ze słowem kluczowym oraz wiadomością, który następnie zapisuje w pliku.
     * Zapis odbywa się pomiędzy na końcu pliku, ale przed chunkiem "IEND"
     * Format chunku: tEXt<słowo kluczowe><null><wiadomość><null><null><null><null>
     * @param message Tekst który ma być ukryty w pliku.
     */
    void PNG::encrypt_message(const std::string &message) {
        int iend_index = find_text_index("IEND", buffer);
        file.seekp(iend_index);
        write_text_to_file(file, CHUNK_NAME);
        write_text_to_file(file, KEYWORD);
        write_text_to_file(file, null, NULL_BYTES_AFTER_KEYWORD);
        write_text_to_file(file, message);
        write_text_to_file(file, null, NULL_BYTES_BEFORE_CHUNK);
        for (int i = iend_index; i < buffer.size(); i++) {
            write_text_to_file(file, buffer[i]);
        }
    }

    /**
     * Odczytuje i wyświetla ukrytą wiadomość znajdując odpowiedni chunk wraz ze słowem kluczowym.
     */
    void PNG::decrypt_message() {
        int chunk_start_index = find_text_index(CHUNK_NAME + KEYWORD, buffer);
        if (chunk_start_index == -1) {
            std::cout << "Wybrany plik nie zawiera ukrytej wiadomosci." << std::endl;
            exit(0);
        }
        int iend_index = find_text_index("IEND", buffer);
        auto start_message_index = chunk_start_index + CHUNK_NAME.size() + KEYWORD.size() + NULL_BYTES_AFTER_KEYWORD;
        auto end_message_index = iend_index - NULL_BYTES_BEFORE_CHUNK;
        for (auto i = start_message_index; i < end_message_index; i++) {
            std::cout << buffer[i];
        }
    }

    /**
     * Wyświetla informację o możliwości zapisania wiadomości w obrazie.
     * W przypadku PNG nie ma zagrożenia zbyt małym rozmiarem obrazu, gdyż
     * zapis odbywa się w osobnym chunku. Sprawdzane jest zatem czy istnieje
     * chunk "IEND", "PNG" oraz czy nie istnieje już zapisana wiadomość.
     * @param message Wiadomość która ma zostać zapisana
     */
    void PNG::check(const std::string &message) {
        int iend_index = find_text_index("IEND", buffer);
        int png_index = find_text_index("PNG", buffer);
        int keyword_index = find_text_index(CHUNK_NAME + KEYWORD, buffer);

        if (iend_index != -1 && png_index != -1 && keyword_index == -1) {
            std::cout << "W pliku mozna umiescic podana wiadomosc" << std::endl;
        } else if(iend_index == -1 || png_index != 1){
            std::cerr << "W pliku nie mozna umiescic podanej wiadomosci, poniewaz plik jest uszkodzony." << std::endl;
        } else {
            std::cout << "W pliku nie mozna umiescic podanej wiadomosci, poniewaz zawiera on inna wiadomosc." << std::endl;
        }
    }

    /**
     * Zczytuje wysokość i szerokość obrazka BMP z bufora.
     * Kolejność bajtów: Big endian
     * @param width Szerokość
     * @param height Wysokość
     */
    void PNG::get_image_dimensions(int &width, int &height) {
        width = (
                buffer[WIDTH_BYTES_INDEX + 0] << 24 |
                buffer[WIDTH_BYTES_INDEX + 1] << 16 |
                buffer[WIDTH_BYTES_INDEX + 2] << 8 |
                buffer[WIDTH_BYTES_INDEX + 3] << 0);
        height = (
                buffer[HEIGHT_BYTES_INDEX + 0] << 24 |
                buffer[HEIGHT_BYTES_INDEX + 1] << 16 |
                buffer[HEIGHT_BYTES_INDEX + 2] << 8 |
                buffer[HEIGHT_BYTES_INDEX + 3] << 0);
    }

    PNG::PNG(const std::string &path) : Image(path) {}

}