#ifndef STEGANOGRAPHY_IMAGE_HPP
#define STEGANOGRAPHY_IMAGE_HPP

#include "unordered_map"
#include <string>
#include <fstream>
#include <vector>

namespace steganography {
    /**
     * Typy obsługiwanych plików graficznych
     */
    enum Extension_type {
        png, bmp
    };

    class Image {
    private:
        /**
         * Zwraca typ rozszerzenia pliku do którego prowadzi ścieżka podana jako argument w postaci pary string:enum
         * @param path Ścieżka do pliku
         * @return Para typu rozszerzenia w postaci string:enum
         */
        static std::pair<const std::basic_string<char>, Extension_type> get_extension(const std::string &path);

    protected:

        /**
         * @return Rozmiar pliku w bajtach.
         */
        std::streamsize get_file_size();

        /**
         * Zwraca Timestamp ostatniej modyfikacji pliku.
         * @param path Ścieżka do pliku.
         * @return Timestamp ostatniej modyfikacji.
         * @return -1 jeśli odczyt się nie powiódł.
         */
        static __time64_t get_modification_timestamp(const std::string &path);

    public:
        std::string path;
        std::fstream file;
        std::vector<unsigned char> buffer;

        /**
         * @param path Ścieżka do pliku
         */
        explicit Image(const std::string &path);

        /**
         * @return Mapa obsługiwanych rozszerzeń jako test oraz odpowiednik jako Enum
         */
        static std::unordered_map<std::string, Extension_type> get_extension_types();

        /**
         * Ukrywa wiadomość w pliku graficznym.
         * @param message Wiadomość która ma zostać ukryta w pliku.
         */
        virtual void encrypt_message(const std::string &message) = 0;

        /**
         * Wyświetla ukrytą wiadomość z pliku.
         */
        virtual void decrypt_message() = 0;

        /**
         * Wyświetla wielkość obrazu, zajmowane miejsce w pamięci oraz timestamp ostatniej modyfikacji.
         */
        void show_info();

        /**
         * Wyświetla informację czy podana wiadomość może zostać zapisana do pliku.
         * @param message Wiadomość która ma zostać zapisana.
         */
        virtual void check(const std::string &message) = 0;

        /**
         * Sprawdza wysokość i szerokość pliku nadpisując zmienne przekazane jako argumenty.
         * @param width Szerokość obrazka
         * @param height Wysokość obrazka
         */
        virtual void get_image_dimensions(int &width, int &height) = 0;

        /**
         * Zwraca Enum rozszerzenia pliku jeśli jest obsługiwane.
         * @param path Ścieżka do pliku.
         * @return Enum rozszerzenia pliku.
         */
        static Extension_type get_extension_as_enum(const std::string &path);

        /**
         * Zwraca reprezentację tekstową rozszerzenia pliku jeśli jest obsługiwane.
         * @param path Ścieżka do pliku.
         * @return Rozszerzenie pliku jako string.
         */
        static std::string get_extension_as_string(const std::string &path);

        /**
         * Zwraca obiekt klasy dziedziczącej po Image na podstawie rozszerzenia pliku podanego w ścieżce.
         * @param path Ścieżka do pliku.
         * @return Obiekt klasy dziedziczącej po Image.
         */
        static Image *create_image(const std::string &path);

        /**
         * Zamienia tekst na ciąg binarny.
         * @param message Wiadomość jako tekst.
         * @return Wiadomość jako ciąg binarny.
         */
        static std::string convert_string_to_binary_string(const std::string &message);


        virtual ~Image();

    };
}


#endif //STEGANOGRAPHY_IMAGE_HPP
