#ifndef STEGANOGRAPHY_BMP_HPP
#define STEGANOGRAPHY_BMP_HPP

#include "Image.hpp"

namespace steganography {

    class BMP : public Image {
    protected:
        int WIDTH_BYTES_INDEX = 18;
        int HEIGHT_BYTES_INDEX = 22;

        /**
        * Zwraca informację o możliwości zapisu podanej wiadomości do pliku.
        * @param message Wiadomość która ma zostać zapisana.
        * @return informację o możliwości zapisu podanej wiadomości do pliku
        */
        bool can_message_fit_in_file(const std::string &message);

    public:
        void encrypt_message(const std::string &message) override;

        void decrypt_message() override;

        void check(const std::string &message) override;

        void get_image_dimensions(int &width, int &height) override;

        /**
         * Tworzy obiekt BMP, zapisuje podaną ścieżkę i na jej podstawie otwiera plik i odczytuje jego zawartość
         * do bufora.
         * @param path Ścieżka do pliku.
         */
        explicit BMP(const std::string &path);
    };
}

#endif //STEGANOGRAPHY_BMP_HPP
