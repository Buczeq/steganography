#ifndef STEGANOGRAPHY_PNG_HPP
#define STEGANOGRAPHY_PNG_HPP


#include "Image.hpp"

namespace steganography {
    class PNG : public Image {
    protected:
        int WIDTH_BYTES_INDEX = 16;
        int HEIGHT_BYTES_INDEX = 20;
    public:
        void encrypt_message(const std::string &message) override;

        void decrypt_message() override;

        void check(const std::string &message) override;

        void get_image_dimensions(int &width, int &height) override;

        /**
        * Znajduje index bajtu, od którego zaczyna się pierwsze wystąpienie szukanego tekstu w liście bajtów.
        * @param text Tekst którego index jest szukany.
        * @param buffer Lista bajtów w której szukany jest tekst
        * @return Index pierwszego wystąpienia szukanego tekstu.
        */
        static int find_text_index(const std::string &text, const std::vector<unsigned char> &buffer);

        /**
         * Tworzy obiekt BMP, zapisuje podaną ścieżkę i na jej podstawie otwiera plik i odczytuje jego zawartość
         * do bufora.
         * @param path Ścieżka do pliku.
         */
        explicit PNG(const std::string &path);

    };
}


#endif //STEGANOGRAPHY_PNG_HPP
