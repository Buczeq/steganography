#include <iostream>
#include <vector>
#include <bitset>
#include <sstream>
#include "bmp.hpp"

namespace steganography {
    static constexpr size_t HEADER_SIZE = 55;
    const std::string KEYWORD = "s23156";

    /**
     * Ukrywa podaną wiadomość w pliku bmp.
     * Wiadomość znajduje się na ostatnich bitach każdego kanału piksela.
     * Ostatnie bity nieużytych pikseli są zerowane.
     * @param message Wiadomość która ma zostać ukryta.
     */
    void BMP::encrypt_message(const std::string &message) {
        if(!can_message_fit_in_file(message)){
            std::cout << "W pliku nie mozna umiescic podanej wiadomosci, poniewaz jest za maly." << std::endl;
            exit(1);
        }
        std::string binaryString = Image::convert_string_to_binary_string(message + KEYWORD);

        unsigned char mask = 0xfe;
        int k = 0;
        for (int j = HEADER_SIZE; j < HEADER_SIZE + binaryString.size(); j++) {
            buffer[j] = (buffer[j] & mask) | (binaryString[k] & 1);
            k++;
        }
        file.seekp(0);
        for (auto v: buffer) {
            file.write(reinterpret_cast<char *>(&v), sizeof(v));
        }
    }

    /**
     * Odczytuje i wyświetla ukrytą wiadomość z pliku bmp.
     * Wiadomość znajduje się na ostatnich bitach każdego kanału piksela.
     */
    void BMP::decrypt_message() {
        std::string binaryString;
        for (int j = HEADER_SIZE; j < buffer.size(); j++) {
            if ((buffer[j] & 1) == 0) {
                binaryString += '0';
            } else {
                binaryString += '1';
            }
        }
        std::stringstream sstream(binaryString);
        std::string result;
        while (sstream.good()) {
                std::bitset<8> bits;
                sstream >> bits;
                char c = char(bits.to_ulong());
                result += c;
        }

        auto found = result.find(KEYWORD);
        if (found == std::string::npos) {
            std::cout << "Wybrany plik nie zawiera ukrytej wiadomosci." << std::endl;
            exit(0);
        }

        std::cout << result.substr(0, found );
    }

    /**
     * Wypisuje informacje o możliwości zapisu podanej informacji do pliku.
     * @param message Wiadomość która ma zostać zapisana.
     */
    void BMP::check(const std::string &message) {
        if (can_message_fit_in_file(message)) {
            std::cout << "W pliku mozna umiescic podana wiadomosc" << std::endl;
        } else {
            std::cout << "W pliku nie mozna umiescic podanej wiadomosci, poniewaz jest za maly." << std::endl;
        }
    }

    bool BMP::can_message_fit_in_file(const std::string &message){
        std::string binaryString = Image::convert_string_to_binary_string(message + KEYWORD);
        return (binaryString.size() <= buffer.size() - HEADER_SIZE);
    }

    /**
     * Odczytuje wysokość i szerokość obrazka BMP z bufora.
     * Kolejność bajtów: Little endian
     * @param width Szerokość
     * @param height Wysokość
     */
    void BMP::get_image_dimensions(int &width, int &height) {
        width = (
                buffer[WIDTH_BYTES_INDEX + 0] << 0 |
                buffer[WIDTH_BYTES_INDEX + 1] << 8 |
                buffer[WIDTH_BYTES_INDEX + 2] << 16 |
                buffer[WIDTH_BYTES_INDEX + 3] << 24);
        height = (
                buffer[HEIGHT_BYTES_INDEX + 0] << 0 |
                buffer[HEIGHT_BYTES_INDEX + 1] << 8 |
                buffer[HEIGHT_BYTES_INDEX + 2] << 16 |
                buffer[HEIGHT_BYTES_INDEX + 3] << 24);
    }

    BMP::BMP(const std::string &path) : Image(path) {}
}
