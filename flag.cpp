#include <iostream>
#include "flag.hpp"

namespace steganography {
    std::unordered_map<std::string, Flag_type> Flag::get_arguments() {
        static std::unordered_map<std::string, Flag_type> arguments = {
                std::make_pair("-i", Flag_type::info),
                std::make_pair("--info", Flag_type::info),
                std::make_pair("-e", Flag_type::encrypt),
                std::make_pair("--encrypt", Flag_type::encrypt),
                std::make_pair("-d", Flag_type::decrypt),
                std::make_pair("--decrypt", Flag_type::decrypt),
                std::make_pair("-c", Flag_type::check),
                std::make_pair("--check", Flag_type::check),
                std::make_pair("-h", Flag_type::help),
                std::make_pair("--help", Flag_type::help)
        };
        return arguments;
    }

    Flag_type Flag::get_argument(const std::string &arg_as_text) {
        auto arguments = get_arguments();
        auto pair = arguments.find(arg_as_text);
        if (pair != arguments.end()) {
            return pair->second;
        } else {
            std::cerr << "Podany argument jest niepoprawny. Uzyj flagi -h aby wyswielic pomoc.";
            exit(1);
        }
    }
}