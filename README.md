# Steganografia / Steganography

[PL]
Finalna wersja projektu semestralnego w C++.

Uproszczone polecenie:

- Zapis i odczyt ukrytych wiadomości w plikach graficznych (png i bmp) 
- Odczyt informacji czy plik zawiera wiadomość
- Odczyt informacji o pliku (rozmiar, czas ostatniej edycji itp.)

[ENG]
Final version of the semester project in C++.

Simplified command:

- Write and read hidden messages in image files (png and bmp)
- Reading information whether the file contains a message
- Read file information (size, last edited time, etc.)
